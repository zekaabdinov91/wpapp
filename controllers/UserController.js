const db = require('../models');
const bcrypt = require('bcrypt');
const jwt  = require('jsonwebtoken');
const fs   = require('fs');
const { body, validationResult } = require('express-validator');
const { unique } = require('../helpers/customValidations');
const config = require('../config/config');
class UserController {

    constructor () {
        this.register = this.register.bind(this);
        this.login = this.login.bind(this)
    }

    async register (req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() });
            return;
        }

        try {
            const { name, country_id, phone, password } = req.body;
            const hash = bcrypt.hashSync(password, 10);

            let user = await db.User.create({
                name: name,
                country_id: country_id,
                phone: phone,
                status: 1,
                password: hash
            });
            return this.responseWithJwtToken(user, res)
        } catch (e) {
            return next(e)
        }
    }

    async login (req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() });
            return;
        }
        const { phone, country_id, password } = req.body;
        try {
            let user = await db.User.findOne({
                where: {
                    phone: phone,
                    country_id: country_id,
                    status: 1
                }
            });
            if(!user)
                res.status(404).json({error: 'User not found'});
            if(bcrypt.compareSync(password, user.password))
                return this.responseWithJwtToken(user, res);
            else
                res.status(400).json({error: 'Password is Invalid'});
        } catch (e) {
            return next(e)
        }
    }


    responseWithJwtToken (user, res) {
        // PRIVATE and PUBLIC key
        let payload = {
            id: user.id,
            name: user.name,
            country_id: user.country_id,
            phone: user.phone,
        };
        let privateKEY  = fs.readFileSync('./private.key', 'utf8');
        let token = jwt.sign(payload, privateKEY, config.jwtSignOptions);
        res.json({
            token: 'Bearer ' + token,
            expiresIn: config.jwtSignOptions.expiresIn
        });
    }


    validate (method) {
        switch (method) {
            case 'Register': {
                return [
                    body('name', "name doesn't exists").exists(),
                    body('phone').isInt().custom(val => unique('User', val, 'phone')),
                    body('country_id').isInt(),
                    body('password').exists().isLength({ min: 6 })
                ]
            }
            case 'Login': {
                return [
                    body('phone').exists(),
                    body('password').exists(),
                    body('country_id').exists()
                ]
            }
        }
    }
}

module.exports = new UserController();
