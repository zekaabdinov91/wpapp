const express = require('express');
const app = express();
let server = require('http').createServer(app);
let io = require('socket.io').listen(server);
const port = process.env.PORT || 3000;
const routes = require('./routes/index.js');

let bodyParser = require('body-parser');
app.use(bodyParser.json());


app.use('/api', routes);

server.listen(port, () => console.log(`Example app listening on port ${port}!`));
