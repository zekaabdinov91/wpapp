const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController');

router.post('/register', UserController.validate('Register'), UserController.register);
router.post('/login', UserController.validate('Login'), UserController.login);

module.exports = router;
