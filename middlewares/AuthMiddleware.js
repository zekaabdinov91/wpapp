let fs = require('fs');
let jwt = require('jsonwebtoken');
let config = require('../config/config');
function authenticate(req, res, next) {
    try{
        let token = req.headers['authorization'].replace(/^Bearer\s/, '');
        let publicKEY  = fs.readFileSync('./public.key', 'utf8');
        let user = jwt.verify(token, publicKEY, config.jwtSignOptions);
        req.user = user;
        next()
    }catch (err) {
        res.status(401).json({error: 'Unauthorized'});
    }
}

module.exports = authenticate;
