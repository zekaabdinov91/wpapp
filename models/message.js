'use strict';
module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define('Message', {
    room_id: DataTypes.INTEGER,
    sender_id: DataTypes.INTEGER,
    message: DataTypes.STRING,
    is_deleted_for_everyone: DataTypes.BOOLEAN
  }, {});
  Message.associate = function(models) {
    // associations can be defined here
  };
  return Message;
};