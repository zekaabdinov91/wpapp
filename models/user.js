'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    country_id: DataTypes.INTEGER,
    phone: DataTypes.INTEGER,
    status: DataTypes.BOOLEAN,
    password: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};
