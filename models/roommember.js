'use strict';
module.exports = (sequelize, DataTypes) => {
  const RoomMember = sequelize.define('RoomMember', {
    chat_room_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER
  }, {});
  RoomMember.associate = function(models) {
    // associations can be defined here
  };
  return RoomMember;
};