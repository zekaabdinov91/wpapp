'use strict';
module.exports = (sequelize, DataTypes) => {
  const MessageReceive = sequelize.define('MessageReceive', {
    message_id: DataTypes.INTEGER,
    receiver_id: DataTypes.INTEGER,
    read_at: DataTypes.DATE
  }, {});
  MessageReceive.associate = function(models) {
    // associations can be defined here
  };
  return MessageReceive;
};