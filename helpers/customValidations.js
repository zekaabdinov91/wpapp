let db = require('../models');
class CustomValidations {
    async unique (model, value, field) {
        let isExists = await db[model].findOne({
            where: { [field]: value}
        });
        if (isExists) {
            throw new Error('Email already registered')
        }
    }
}

module.exports = new CustomValidations();
